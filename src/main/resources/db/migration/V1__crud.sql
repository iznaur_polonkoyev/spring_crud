CREATE TABLE crud.person

(
    id          SERIAL PRIMARY KEY,
    name        VARCHAR(255),
    surname     VARCHAR(255),
    patronymic  VARCHAR(255),
    date_create timestamp default current_timestamp,
    date_update timestamp default current_timestamp
);

CREATE TABLE crud.contact
(
    id           SERIAL PRIMARY KEY,
    phone_number VARCHAR(255),
    type         VARCHAR(255),
    person_id    INTEGER,
    CONSTRAINT fk_person
        FOREIGN KEY (person_id)
            REFERENCES crud.person (id)
            ON DELETE CASCADE
             ON UPDATE CASCADE,
    date_create  timestamp default current_timestamp,
    date_update  timestamp default current_timestamp

);
