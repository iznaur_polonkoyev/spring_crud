package kz.company.task.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "contact", schema = "crud")
public class ContactEntity {

    @Id
    @Column(name = "id", updatable = false)
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "contact_seq")
    @SequenceGenerator(
            name = "contact_seq",
            sequenceName = "contact_id_seq", schema = "crud", allocationSize = 1)
    private Integer id;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "type")
    private String type;

    @Column(name = "date_create", updatable = false)
    private LocalDateTime dateCreate;

    @Column(name = "date_update")
    private LocalDateTime dateUpdate;

    @Column(name = "person_id", updatable = false)
    private Integer personId;

    @PrePersist
    public void toCreate() {
        setDateCreate(LocalDateTime.now());
        setDateUpdate(getDateCreate());
    }

    @PreUpdate
    public void toUpdate() {
        setDateUpdate(LocalDateTime.now());
    }

}
