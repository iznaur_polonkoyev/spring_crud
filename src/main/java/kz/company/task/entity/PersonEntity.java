package kz.company.task.entity;

import javax.persistence.*;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
@ApiModel(value = "PersonEntity")
@Table(name = "person", schema = "crud")
public class PersonEntity {

    @Id
    @Column(name = "id", updatable = false)
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "person_seq")
    @SequenceGenerator(
            name = "person_seq",
            sequenceName = "person_id_seq", schema = "crud", allocationSize = 1)
    private Integer id;

    @Column(name = "name")
    @ApiModelProperty(value = "name of person", example = "Iznaur")
    private String name;

    @Column(name = "surname")
    @ApiModelProperty(value = "surname of person", example = "Polonkoev")
    private String surname;

    @Column(name = "patronymic")
    @ApiModelProperty(value = "surname of person", example = "Bekhanovich")
    private String patronymic;

    @Column(name = "date_create", updatable = false)
    private LocalDateTime dateCreate;

    @Column(name = "date_update")
    private LocalDateTime dateUpdate;

    @PrePersist
    public void toCreate() {
        setDateCreate(LocalDateTime.now());
        setDateUpdate(getDateCreate());
    }

    @PreUpdate
    public void toUpdate() {
        setDateUpdate(LocalDateTime.now());
    }

    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    @JoinColumn(name = "person_id")
    private List<ContactEntity> contacts;

}
