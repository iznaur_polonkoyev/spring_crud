package kz.company.task.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import io.swagger.annotations.ApiOperation;
import kz.company.task.entity.ContactEntity;
import kz.company.task.entity.PersonEntity;
import kz.company.task.model.ContactDto;
import kz.company.task.model.PersonDto;
import kz.company.task.repository.ContactRepository;
import kz.company.task.repository.PersonRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class PersonService {

    private final PersonRepository personRepository;
    private final ContactRepository contactRepository;
    private final ModelMapper modelMapper;

    @ApiOperation(value = "")
    public PersonDto createPerson(PersonDto dto) {

        PersonEntity personEntity = personRepository
                .save(modelMapper.map(dto, PersonEntity.class));
        return modelMapper.map(personEntity, PersonDto.class);
    }

    @Transactional
    public PersonDto updatePerson(PersonDto dto) {
        boolean result = personRepository.existsById(dto.getId());

        if (!result) {
            log.warn("Error :{}", dto.getId());
            throw new RuntimeException("ERROR");
        }

        return this.createPerson(dto);
    }

    @Transactional
    public boolean deletePerson(Integer id) {
        personRepository.deleteById(id);

        Optional<PersonEntity> person = personRepository.findById(id);

        return !person.isPresent();
    }

    public PersonDto getPersonById(Integer id) {
        Optional<PersonEntity> optional = personRepository.findById(id);
        return optional.map(item -> modelMapper.map(item, PersonDto.class))
                .orElse(null);
    }

    public List<PersonDto> getPersons() {
        List<PersonEntity> entities = personRepository.findAll();
        return entities.stream()
                .map(item -> modelMapper.map(item, PersonDto.class))
                .collect(Collectors.toList());
    }

}
