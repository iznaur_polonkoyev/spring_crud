package kz.company.task.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@ToString
@NoArgsConstructor
public class ContactDto {

    private Integer id;

    private String phoneNumber;

    private String type;

    private Integer personId;

    private LocalDateTime date_create;

    private LocalDateTime date_update;

}



