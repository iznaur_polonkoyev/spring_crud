package kz.company.task.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@ToString
@NoArgsConstructor
public class PersonDto {

    private Integer id;

    private String name;

    private String surname;

    private String patronymic;

    private LocalDateTime date_create;

    private LocalDateTime date_update;

    private List<ContactDto> contacts;
}



