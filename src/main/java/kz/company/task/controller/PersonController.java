package kz.company.task.controller;

import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kz.company.task.entity.PersonEntity;
import kz.company.task.model.ContactDto;
import kz.company.task.model.PersonDto;
import kz.company.task.service.PersonService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@RestController
@RequiredArgsConstructor
@EnableSwagger2
@RequestMapping(path = "/person")
@Api(value = "crud operations")
public class PersonController {

    private final PersonService personService;

    @PostMapping(path = "/create")
    @ApiOperation(value = "create person", response = PersonEntity.class)

    private ResponseEntity<PersonDto> createPerson(@RequestBody PersonDto dto) {
        return ResponseEntity.ok(personService.createPerson(dto));
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "update person", response = PersonEntity.class)
    private ResponseEntity<PersonDto> updatePerson(@RequestBody PersonDto dto) {
        return ResponseEntity.ok(personService.updatePerson(dto));
    }

    @DeleteMapping(path = "/delete")
    @ApiOperation(value = "delete person", response = PersonEntity.class)
    private ResponseEntity<?> deletePerson(@RequestParam(name = "id") Integer id) {
        personService.deletePerson(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "show all persons", response = Iterable.class)
    private ResponseEntity<List<PersonDto>> getPersons() {
        return ResponseEntity.ok(personService.getPersons());
    }

    @GetMapping(path = "/id")
    @ApiOperation(value = "show person by id", response = PersonEntity.class)
    private ResponseEntity<PersonDto> getPersonById(@RequestParam(name = "id") Integer id) {
        return ResponseEntity.ok(personService.getPersonById(id));
    }

}
