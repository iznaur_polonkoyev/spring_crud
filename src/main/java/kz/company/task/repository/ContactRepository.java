package kz.company.task.repository;

import kz.company.task.entity.ContactEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContactRepository extends JpaRepository<ContactEntity, Integer> {

    List<ContactEntity> findAllByPersonId(Integer id);

    void deleteAllByPersonIdIsNull();
}
