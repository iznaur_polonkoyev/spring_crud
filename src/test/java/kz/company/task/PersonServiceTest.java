package kz.company.task;

import kz.company.task.generator.EntityGenerator;
import kz.company.task.model.ContactDto;
import kz.company.task.model.PersonDto;
import kz.company.task.repository.PersonRepository;
import kz.company.task.service.PersonService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class PersonServiceTest {

    @Autowired
    public PersonService personService;

    @Autowired
    public EntityGenerator entityGenerator;

    @Autowired
    public PersonRepository personRepository;

    @Test
    public void createPerson() {
        PersonDto personDto = entityGenerator.generatePerson();

        personDto.setContacts(entityGenerator.generateContacts(3));
        personDto = personService.createPerson(personDto);

        assertNotNull(personDto);
        assertEquals(personDto.getContacts().size(), 3);
    }

    @Test
    public void updatePerson() {
        PersonDto personDto = entityGenerator.generatePerson();

        personDto.setContacts(entityGenerator.generateContacts(3));
        personDto = personService.createPerson(personDto);

        PersonDto oldPerson = entityGenerator.generatePerson();
        oldPerson.setId(personDto.getId());
        oldPerson.setContacts(entityGenerator.generateContacts(3));
        PersonDto newPerson = personService.updatePerson(oldPerson);

        assertEquals(oldPerson.getId(), personDto.getId());
        assertEquals(oldPerson.getName(), newPerson.getName());
        assertNotEquals(newPerson.getName(), personDto.getName());
        assertEquals(oldPerson.getSurname(), newPerson.getSurname());
        assertNotEquals(newPerson.getSurname(), personDto.getSurname());
        assertEquals(oldPerson.getPatronymic(), newPerson.getPatronymic());
        assertNotEquals(newPerson.getPatronymic(), personDto.getPatronymic());
        assertNotNull(personDto);
        assertEquals(personDto.getContacts().size(), 3);
    }

    @Test
    public void deletePerson() {
        PersonDto personDto = entityGenerator.generatePerson();

        personDto.setContacts(entityGenerator.generateContacts(3));
        personDto = personService.createPerson(personDto);

        Boolean result = personService.deletePerson(personDto.getId());

        assertEquals(result, true);
    }

    @Test
    public void getById() {
        PersonDto personDto = entityGenerator.generatePerson();
        personDto.setContacts(entityGenerator.generateContacts(3));
        personDto = personService.createPerson(personDto);

        PersonDto findPerson = personService.getPersonById(personDto.getId());
        assertEquals(personDto.getId(), findPerson.getId());
        assertEquals(personDto.getName(), findPerson.getName());
        assertEquals(personDto.getSurname(), findPerson.getSurname());
        assertEquals(personDto.getPatronymic(), findPerson.getPatronymic());

    }

    @Test
    public void getByAll() {

        int personSize = 5;
        personRepository.deleteAll();

        ArrayList<PersonDto> addPerson = new ArrayList();

        for (int i = 0; i < personSize; i++) {
            PersonDto personDto = entityGenerator.generatePerson();
            personDto.setContacts(entityGenerator.generateContacts(3));
            personDto = personService.createPerson(personDto);
            addPerson.add(personDto);
        }

        List<PersonDto> allPersons = personService.getPersons();

        for (int i = 0; i < personSize; i++) {
            PersonDto createdPerson = addPerson.get(i);
            PersonDto returnedPerson = allPersons.get(i);

            assertNotNull(createdPerson);
            assertNotNull(returnedPerson);
            assertEquals(createdPerson.getId(), returnedPerson.getId());
            assertEquals(createdPerson.getName(), returnedPerson.getName());
            assertEquals(createdPerson.getSurname(), returnedPerson.getSurname());
            assertEquals(createdPerson.getPatronymic(), returnedPerson.getPatronymic());
            assertNotNull(createdPerson.getContacts());
            assertNotNull(returnedPerson.getContacts());
            assertEquals(createdPerson.getContacts().size(), returnedPerson.getContacts().size());
            for (int j = 0; j < returnedPerson.getContacts().size(); j++) {
                ContactDto createdContact = createdPerson.getContacts().get(j);
                ContactDto returnedContact = returnedPerson.getContacts().get(j);
                assertNotNull(createdContact);
                assertNotNull(returnedContact);
                assertEquals(createdContact.getPhoneNumber(), returnedContact.getPhoneNumber());
                assertEquals(createdContact.getType(), returnedContact.getType());
            }
        }

    }

}
