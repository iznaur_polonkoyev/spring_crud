package kz.company.task.generator;

import io.swagger.models.Contact;
import kz.company.task.model.ContactDto;
import kz.company.task.model.PersonDto;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
public class EntityGenerator {

    public PersonDto generatePerson() {
        PersonDto personDto = new PersonDto();
        personDto.setName(randomString());
        personDto.setSurname(randomString());
        personDto.setPatronymic(randomString());
        return personDto;
    }

    public String randomString() {
        return RandomStringUtils.random(10, true, false);
    }

    public List<ContactDto> generateContacts(int contactSize) {
        ArrayList<ContactDto> contacts = new ArrayList();

        ArrayList<String> list = new ArrayList();
        list.add("MOB");
        list.add("HOME");
        list.add("WORK");


        for (int i = 0; i < contactSize; i++) {
            int index = new Random().nextInt(list.size());
            String text = list.get(index);
            contacts.add(generateContact(text));
        }
        return contacts;
    }


    public ContactDto generateContact(String text) {
        ContactDto contactDto = new ContactDto();
        String number = null;
        switch (text) {
            case "MOB":

                number = "747" + RandomStringUtils.random(7, false, true);
                contactDto.setType("MOB");

                break;
            case "HOME":

                number = "2" + RandomStringUtils.random(6, false, true);
                contactDto.setType("HOME");

                break;
            case "WORK":

                number = "727" + RandomStringUtils.random(10, false, true);
                contactDto.setType("WORK");

                break;
        }
        contactDto.setPhoneNumber(number);
        return contactDto;
    }
}
